<?php
$eau = '~';
$bateau = 'O';
$touche = 'X';
$ocean = array();

$DEBUG = true;

srand(microtime(true));

function questionner_utilisateur($demanderInformationsBateau=FALSE, $hauteurMax=40, $largeurMax=80){
    global $lignes, $colonnes, $bateauPosH, $bateauPosV, $bateauDirection, $bateauTaille, $DEBUG;

    print($DEBUG ? "DEBUG: hauteurMax=$hauteurMax\n":"");
    print($DEBUG ? "DEBUG: largeurMax=$largeurMax\n":"");
    do{
        $lignes = readline("Nombre de lignes [1..$hauteurMax] : ");
        print($DEBUG ? "DEBUG: lignes=$lignes\n":"");
    }
    while($lignes < 1 or $lignes > $hauteurMax);

    do{
        $colonnes = readline("Nombre de colonnes [1..$largeurMax] : ");
        print($DEBUG ? "DEBUG: colonnes=$colonnes\n":"");
    }
    while($colonnes < 1 or $colonnes > $largeurMax);

    do{
        if($demanderInformationsBateau){
            $question = "Position verticale du bateau [0..".($lignes - 1)."] : ";
            $bateauPosV = readline($question);
        }
        else{
            $bateauPosV = rand(0, $lignes - 1);
        }
        print($DEBUG ? "DEBUG: bateauPosV=$bateauPosV\n":"");
    }
    while($bateauPosV < 0 or $bateauPosV >= $lignes);

    do{
        if($demanderInformationsBateau){
            $question = "Position horizontale du bateau [0..".($colonnes - 1)."] : ";
            $bateauPosH = readline($question);
        }
        else{
            $bateauPosH = rand(0, $colonnes - 1);
        }
        print($DEBUG ? "DEBUG: bateauPosH=$bateauPosH\n":"");
    }
    while($bateauPosH < 0 or $bateauPosH >= $colonnes);

    do{
        if($demanderInformationsBateau){
            $bateauDirection = readline("Direction du bateau (h|v) : ");
        }
        else{
            $bateauDirection = ((rand() % 2) == 0) ? 'h' : 'v';
        }
        print($DEBUG ? "DEBUG: bateauDirection=$bateauDirection\n":"");
    }
    while($bateauDirection != 'h' and $bateauDirection != 'v');

    if($bateauDirection == 'h'){
        $bateauTailleMax = $colonnes - 1 - $bateauPosH;
    }
    else{
        $bateauTailleMax = $lignes - 1 - $bateauPosV;
    }
    print($DEBUG ? "DEBUG: bateauTailleMax=$bateauTailleMax\n":"");

    do{
        if($demanderInformationsBateau){
            $bateauTaille = readline("Taille du bateau [0..$bateauTailleMax] : ");
        }
        else{
            $bateauTaille = rand(0, $bateauTailleMax);
        }
        print($DEBUG ? "DEBUG: bateauTaille=$bateauTaille\n":"");
    }
    while($bateauTaille < 0 or $bateauTaille > $bateauTailleMax);
}

function initialiser_carte($hauteur, $largeur){
    if($hauteur > 0 and $largeur > 0){
        global $eau;
        $carte = array();
        for($i=0; $i<$hauteur; $i++){
            $ligne = array();
            for($j=0; $j<$largeur; $j++){
                $ligne[] = $eau;
            }
            $carte[] = $ligne;
        }
        return $carte;
    }
    else{
        return FALSE;
    }
}

function afficher_carte($carte){
    global $lignes, $colonnes;
    for($i=0; $i<$lignes; $i++){
        for($j=0; $j<$colonnes; $j++){
            print($carte[$i][$j]);
        }
        print("\n");
    }
}

function positionner_bateau($carte, $posVerticale, $posHorizontale, $direction, $taille){
    global $lignes, $colonnes, $bateau, $DEBUG;
    if($direction == 'h'){
	    // verifier que le bateau tient horizontalement
	    $posHorizontaleFin = $posHorizontale + $taille;
	    if($posHorizontaleFin <= $colonnes){
            print($DEBUG ? "DEBUG: bateau horizontal de ($posVerticale,$posHorizontale) à ($posVerticale,$posHorizontaleFin)\n":"");
            for($j=$posHorizontale; $j<=$posHorizontaleFin; $j++){
                $carte[$posVerticale][$j] = $bateau;
            }
	    }
    }
    elseif($direction == 'v'){
	    // verifier que le bateau tient verticalement
	    $posVerticaleFin = $posVerticale + $taille;
	    if($posVerticaleFin <= $lignes){
            print($DEBUG ? "DEBUG: bateau vertical de ($posVerticale,$posHorizontale) à ($posVerticaleFin,$posHorizontale)\n":"");
            for($i=$posVerticale; $i<=$posVerticaleFin; $i++){
                $carte[$i][$posHorizontale] = $bateau;
            }
	    }
    }
    return $carte;
}


///////////////////////////////////////
// Programme principal
///////////////////////////////////////
if(isset($argc)){
    questionner_utilisateur(true);
    $ocean = initialiser_carte($lignes, $colonnes);
    if($ocean){
        $ocean = positionner_bateau($ocean, $bateauPosV, $bateauPosH, $bateauDirection, $bateauTaille);
        afficher_carte($ocean);
    }
    else{
        print("Une erreur est survenue ; la carte navale n'existe pas.\n");
    }
}
else{
    print("<!DOCTYPE html><html><body><h1>Vous devez exécuter ce programme dans un shell et non pas dans un navigateur Web.</h1>\n");
    print("<h2>En guise d'information, voici le code source du programme.</h2>");
    highlight_file("batailleNavale.php");
    print("</body></html>");
}

?>
